import React from "react";

const Button = ({ background, text, onClick }) => {
  const style = {
    backgroundColor: background,
    borderRadius: "20px",
    padding: "10px",
    fontWeight: "bold",
  };

  return (
    <button onClick={onClick} style={style}>
      {text}
    </button>
  );
};

Button.defaultProps = {
  background: "rgb(181, 69, 69)",
};

export default Button;
