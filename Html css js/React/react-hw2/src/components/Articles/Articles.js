import React from 'react'
import axios from 'axios'
import Article from '../Article/Article'
import './Articles.scss'


class Articles extends React.Component {
    
    
    
    async componentDidMount() {
        const {setArticles} = this.props;
        const articles =  await axios('/articles.json')
        setArticles(articles.data);
        

        
        }
        
    render() { 
        const {articles, addToFavorite, favorite, openModal, closeModal, addToCart, cart} = this.props;
        const arrayOfArticles = articles.map((article, index) => <Article article={article} key={index} index={index} addToFavorite={addToFavorite} favorite={favorite} openModal={openModal} closeModal={closeModal} addToCart={addToCart} cart={cart}/>)



        return <div class='allArticles'>{arrayOfArticles}</div>
    }
}


export default Articles;