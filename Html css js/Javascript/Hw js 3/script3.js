

function calculate (number1, number2, action) {
    switch(action) {
        case '+': return `${number1} ${action} ${number2} = ${number1 + number2}`;
        case '-': return `${number1} ${action} ${number2} = ${number1 - number2}`;
        case '/': return `${number1} ${action} ${number2} = ${number1 / number2}`;
        case '*': return `${number1} ${action} ${number2} = ${number1 * number2}`;
        default: return `Error, ${action} is unknown operation`;
    }
}

function askNumber() {
    do {
        number = +prompt("Enter your number");
    } while(isNaN(number));
    return number;
}
    
let num1 = askNumber();
let num2 = askNumber();
let sign = prompt("Enter operation (+ - * /)");


console.log(calculate(num1, num2, sign));
