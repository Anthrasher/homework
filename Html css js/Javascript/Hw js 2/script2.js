let range;

do{
    range = +prompt("Enter your number (more than 0)")

} while (isNaN(range) || range < 0 || parseInt(range) != parseFloat(range))


if (range < 5){
    console.log("Sorry, no numbers");
}
else {
    for (let i = 0; i <= range; i++){
        if (i % 5 === 0){
            console.log(i);
        }
    }
}