const themeButton = document.createElement('button');
themeButton.innerText = 'Сменить тему'
themeButton.onclick = function() {
    const darkTheme = `
    background: black;
    --main-description-background: grey;
    --main-text-color: white;
    `
    const whiteTheme = `
    background: white;
    --main-description-background: #F9F7F7;
    --main-text-color: black;
    `

    if (document.body.style.background === 'white') {
        document.body.style.cssText = darkTheme;
        return;
    }
    else {
        document.body.style.cssText = whiteTheme;
        return;
    }
}

document.body.prepend(themeButton);