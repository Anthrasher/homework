let x = 5
const testArray = [ 23, '23', 'hhello', true, 345, NaN, x];

function filterBy(array, filterType) {
  return array.filter((item) => (typeof(item) !== filterType))
}

const res = filterBy(testArray, 'number');
console.log(res);