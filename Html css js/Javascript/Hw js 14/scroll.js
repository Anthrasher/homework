function addToTheTopButton() {
const toTheTopButton = document.createElement('button');
    toTheTopButton.style.cssText = `
   background: #FF9800;   
   width: 50px;   
   height: 50px;  
   border-radius: 50%;   
   margin: 30px;   
   position: fixed;   
   bottom: 40px;   
   right: 40px;   
   transition: background-color .3s;   
   z-index: 1000;
   outline:none;
   display:none;
    `;
    toTheTopButton.innerHTML = '&#129145;';
    toTheTopButton.id = 'toTopButton';
    toTheTopButton.onclick = function() {
        window.scrollTo({
            top:0, 
            behavior: 'smooth',
        }
        )
    }
    document.body.append(toTheTopButton);
}

function addHideButton() {
    const popular = document.querySelector('.popular');
const hideButton = document.createElement('button');
hideButton.innerText = 'Hide/show section';
hideButton.style.cssText = `
background:orangered;
color:black;
padding:10px;
`
popular.after(hideButton);

hideButton.onclick = function() {
    if (popular.style.display === 'none') {
    popular.style.display = 'block';
    }
    else {
        popular.style.display = 'none';
    }
}
}

addToTheTopButton();
addHideButton();

window.addEventListener('scroll', function(event) {
    const toTheTopButton = document.querySelector('#toTopButton');
    if (window.pageYOffset > 900) {
        toTheTopButton.style.display = 'inline';
    }
    else {
        toTheTopButton.style.display = 'none';
    }
})
