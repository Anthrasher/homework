const list = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", null, undefined, 0, '0']; 

function showList(list, parent = document.body) {
    const fragment = new DocumentFragment()
    const ul = document.createElement('ul');
    list.map(item => {
        if(item) {
            const li = document.createElement('li');
            li.innerText = item;
            ul.append(li);
        }
    });
    fragment.append(ul);
    parent.append(fragment);
}


showList(list);