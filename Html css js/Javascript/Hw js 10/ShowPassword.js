const elements = {
    passwordInput: document.querySelector('#password'),
    confirmPassword: document.querySelector('#confirmPassword'),
    submitButton:document.querySelector('button'),
    WARNING: document.createElement('p'),
    showPasswordIcon: document.querySelector('#password-icon'),
    showConfirmIcon: document.querySelector('#confirm-icon')
}

function showPassword() {
    let input;
    if (event.target === elements.showPasswordIcon) {
        input = elements.passwordInput;
    }
    else {
        input = elements.confirmPassword;
    }

    if (input.type === 'password') {
        input.type = 'text';
        event.target.src = 'imgs/disabledEye.png'
    }
    else {
        input.type = 'password';
        event.target.src = 'imgs/showEye.png'

    }

}

function checkPasswords() {
    if (elements.passwordInput.value === elements.confirmPassword.value) {
        alert("Добро пожаловать!")
    }
    else {
        elements.WARNING.innerText = 'Нужно ввести одинаковые значения';
        elements.WARNING.style.color = 'red'
        document.querySelector('.password-form').after(elements.WARNING);
    }
}

elements.showPasswordIcon.addEventListener('click', showPassword)
elements.showConfirmIcon.addEventListener('click', showPassword)

elements.submitButton.addEventListener('click', checkPasswords)

