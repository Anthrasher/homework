let now = new Date();

function createNewUser() {
    let userName;
    let userSurname;
    let birthDate;

    while(!userName) {
        userName = prompt("Enter your name");
    }

    while(!userSurname){
        userSurname = prompt("Enter your surname");
    }
    birthDate = prompt("Enter birth date (dd.mm.yyyy)")
    let splitDate = birthDate.split('.')
    return newUser = {
        firstName: userName,
        lastName: userSurname,
        birthDate: birthDate,
        getPassword() {
            return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${splitDate[2]}`;
        },
        getAge() {
            age = now.getFullYear() - splitDate[2];
            if (now.getMonth() > (splitDate[1]-1)) {
                return age;
            }
            if (now.getMonth() === (splitDate[1]-1)) {
                if(now.getDate() >= splitDate[0]) {
                    return age
                }
            }
            return age - 1;
        },
        setFirstname(newName) {
            this.firstName = newName;
        },
        setLastname(newLastame) {
            this.lastName = newLastame;
        }

    }
}


console.log(createNewUser());
console.log(newUser.getPassword());
newUser.setFirstname("Tarzen");
console.log(newUser.firstName);
newUser.setLastname("Tarzenenko");
console.log(newUser.lastName);
console.log(newUser.getAge());