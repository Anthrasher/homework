const priceInput = document.getElementById('price');
const div = document.createElement('div')
const price = document.createElement('span');
const clearButton = document.createElement('button');

function clearSpan() {
  div.remove();
  priceInput.value = '';
}



function greenBorder() {
  priceInput.style.cssText = `
  border: 2px solid green;
  outline: none`
}

function inputOnBlur() {
  priceInput.style.border = "1px solid grey";
  if(priceInput.value >= 1) {
      clearButton.innerText = 'x';
      clearButton.onclick = clearSpan;
      clearButton.style.borderRadius = '50%'
      price.innerText = `Текущая цена: ${priceInput.value} грн`
      priceInput.style.color = 'green';
      div.append(price, clearButton)
    document.body.prepend(div);
  }
  else {
    price.innerText = 'Пожалуйста введите коректную цену'
    priceInput.style.color = 'red';
    div.append(price);
    clearButton.remove();
    priceInput.after(div);
  }
  
}


priceInput.onfocus = greenBorder;
priceInput.onblur = inputOnBlur;