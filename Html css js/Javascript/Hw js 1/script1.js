let userName;
let userAge;
const DENY = `You are not allowed to visit this site`;


do {
    userName = (prompt ("Enter your name", userName));
    if (userName === null){
        continue;
    }
    else {
        userName = userName.trim();
    }
} while(!userName || typeof(userName) !== 'number')

do { 
    userAge = +prompt ("Enter your age", userAge);
} while(isNaN(userAge))

const ACCESS = `Welcome, ${userName}!`;

if (userAge < 18){
    alert(DENY);
}
else if (userAge <= 22){
    confirm("Are you sure you want to continue?") ? alert(ACCESS) : alert(DENY);
}
else{
    alert(ACCESS);
}