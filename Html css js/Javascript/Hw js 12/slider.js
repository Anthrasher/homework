const imgs = document.querySelectorAll('img');
let counter = 0;

function slideShow() {
    for (let img of imgs) {
        img.classList.add('hidden')
        imgs[counter % 4].classList.remove('hidden');
    }
    counter++
}

let timerId = setInterval(slideShow, 3000)

const stopButton = document.createElement('button');
stopButton.innerText = 'Приостановить';
stopButton.onclick = function() {
    clearInterval(timerId)
    continueButton.removeAttribute('disabled');
    this.setAttribute('disabled', '');
}


const continueButton = document.createElement('button');
continueButton.innerText = 'Возобновить';
continueButton.setAttribute('disabled', 'disabled')
continueButton.onclick = function() {
    timerId = setInterval(slideShow, 3000);
    stopButton.removeAttribute('disabled');
    this.setAttribute('disabled', '')
}

document.body.after(continueButton);
document.body.after(stopButton);

