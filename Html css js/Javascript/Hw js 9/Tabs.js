const tabs = document.querySelector('#tabs');
const buttonsList = document.querySelector('.tabs');

function setTargetTabActive() {
    for (let child of tabs.children) {
        child.style.display = 'none';
    }
    document.querySelector(`.${event.target.id}`).style.display = 'block'
}

function setTargetButtonActive() {
    for(let child of buttonsList.children) {
        child.classList.remove('active');
    }
    event.target.classList.add('active');
}



buttonsList.onclick = function(event) {
        setTargetTabActive();
        setTargetButtonActive();
    }