function createNewUser() {
    let userName;
    let userSurname;

    while(!userName) {
        userName = prompt("Enter your name");
    }

    while(!userSurname){
        userSurname = prompt("Enter your surname");
    }
    return newUser = {
        firstName: userName,
        lastName: userSurname,
        getLogin() {
            return `${this.firstName[0]}${this.lastName}`.toLowerCase();
        },
        setFirstname(newName) {
            this.firstName = newName;
        },
        setLastname(newLastame) {
            this.lastName = newLastame;
        }
    }
}


console.log(createNewUser());
console.log(newUser.getLogin());
newUser.setFirstname("Tarzen");
console.log(newUser.firstName);
newUser.setLastname("Tarzenenko");
console.log(newUser.lastName);