class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set salary(newValue) {
        this._salary = newValue;
    }

    get salary() {
        return this._salary;
    }

    set name(newName) {
        this._name = newName;
    }

    get name() {
        return this._name
    }

    set age(newAge) {
        this._age = newAge;
    }

    get age() {
        return this._age;
    }
}

class Programmer extends Employee {
    constructor(lang, ...parentParams) {
        super(...parentParams);
        this.lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }

    set salary(newValue) {
        super.salary = newValue;
    }
}


const testProgrammer = new Programmer('English', 'Ivan', 22, 400);
const anotherProgrammer = new Programmer ('Deutsch', 'Anna', 24, 500);

console.log(testProgrammer, anotherProgrammer)
