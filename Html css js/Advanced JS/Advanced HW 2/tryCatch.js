const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    },
    {
      price:20
    },
    {
      author:'tarzan',
      name: 'test',
      price: 123,
    }
  ];

const div = document.querySelector('#root');

let neededInfo = ['author','name','price'];



books.forEach((book) => {
  try{
          neededInfo.forEach((prop) => {
            if(!book[prop]) {
              throw new Error(`This book dont have ${prop}`);
            }
          }) 

          const ul = document.createElement('ul');
          for (prop in book) {
            const li = document.createElement('li');
            li.innerText = `${prop}: ${book[prop]}`
            ul.append(li)
          }
          div.append(ul)
        }
      catch(error) {  
          console.log(error)
      }
    })
  

//   const minimalInfo = 3
//   const preparedBooks = books.filter((book) => Object.keys(book).length === minimalInfo)
//   console.log(preparedBooks);

//   preparedBooks.forEach((book) => {
//       const ul = document.createElement('ul');
//       for (prop in book) {
//           const li = document.createElement('li');
//           li.innerText = `${prop}: ${book[prop]}`
//           ul.append(li)
//       }
//       div.append(ul)
//   })
  