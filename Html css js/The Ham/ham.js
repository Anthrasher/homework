const elements = {
    tabMenu: document.querySelector('.services-menu'),
    tabButtons: document.querySelectorAll('.services-menu-tab'),
    tabs: document.querySelectorAll('.services-tab'),

    filterMenu: document.querySelector('.work-menu'),
    filterMenuButtons: document.querySelectorAll('.work-menu-tab'),
    imagesToFilter: document.querySelectorAll('.work-img-card'),
    loadMore: document.querySelector('.load-button'),

    imgsPerPage:12,
}

function hideGroup(element) {
    element.forEach(elem => elem.classList.add('hidden'));
}

function hideElement(element) {
    element.classList.add('hidden');
}

function showElement(element) {
    element.classList.remove('hidden');
}

function toggleTabActive(menu, activeClass) {
    menu.forEach(button => button.classList.remove(activeClass));
    event.target.classList.add(activeClass);
}

elements.tabMenu.addEventListener('click', event => {
    hideGroup(elements.tabs);
    showElement(elements.tabs[+event.target.id - 1]);
    toggleTabActive(elements.tabButtons, 'active');
    
})

elements.filterMenu.addEventListener('click', event => { 
    hideGroup(elements.imagesToFilter);
    toggleTabActive(elements.filterMenuButtons, 'work-menu-active');
    if (event.target.innerText === 'All') {
        showElement(elements.loadMore);
        elements.imagesToFilter.forEach((elem, index) => {
        if (index < elements.imgsPerPage) {
            showElement(elem);
        }
        })
    }
    else {
        const filterClass = event.target.innerText.toLowerCase().split(' ').join('-');
        elements.imagesToFilter.forEach(image => {
            if (image.classList.contains(filterClass)) {
                showElement(image);
            }
        })
        hideElement(elements.loadMore);
    }
})

elements.loadMore.addEventListener('click', event => {
    elements.imagesToFilter.forEach((elem, index) => {
        if (index < elements.imgsPerPage*2) {
            showElement(elem);
        }
        })
    hideElement(elements.loadMore);
})








document.addEventListener( 'DOMContentLoaded', function () {
    let navigationSlider = new Splide ('#secondary-slider', {
        fixedWidth: 60,
        fixedHeight: 60,
		rewind    : true,
		gap       : 40,
		pagination: false,
        isNavigation: true,
        classes:{
            arrows: 'splide__arrows square-arrows',
            arrow : 'splide__arrow square-arrow',
		    prev  : 'splide__arrow--prev your-class-prev',
		    next  : 'splide__arrow--next your-class-next',
	},
    }).mount();
	let reviewSlider = new Splide( '#image-slider', {
        arrows:false,
        pagination:false,
    });


    reviewSlider.sync(navigationSlider).mount();
    navigationSlider.on('moved' ,function() {
        const slides = document.querySelectorAll('.navigation-slide');
        slides.forEach(slide => {
            slide.style.boxShadow = '0px 0px 0px 6px rgba(31, 218, 181, 0.2)'
            if (slide.parentElement.classList.contains('is-active')) {
                slide.style.boxShadow = '0px 0px 0px 6px #18CFAB'
            }
        })

    })
} );
